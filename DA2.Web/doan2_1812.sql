CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BILL]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BILL](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [int] NULL,
	[created] [datetime] NULL,
	[customer_id] [int] NULL,
	[amount] [float] NULL,
	[nguoi_nhan] [nvarchar](50) NULL,
	[dia_chi] [nvarchar](200) NULL,
	[so_dt] [nvarchar](20) NULL,
 CONSTRAINT [PK_BILL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BILLITEM]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BILLITEM](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bill_id] [int] NULL,
	[product_id] [int] NULL,
	[quantity] [int] NULL,
	[price] [float] NULL,
 CONSTRAINT [PK_BILLITEM] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BRAND]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BRAND](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[alias] [nvarchar](100) NULL,
	[category_id] [int] NULL,
	[deleted] [bit] NULL,
	[deleted_date] [date] NULL,
 CONSTRAINT [PK_BRAND] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CATEGORY]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CATEGORY](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[alias] [nvarchar](100) NULL,
	[deleted] [bit] NULL,
	[deleted_date] [date] NULL,
 CONSTRAINT [PK_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[email] [nvarchar](100) NULL,
	[password] [nvarchar](100) NULL,
	[tel] [nvarchar](100) NULL,
	[address] [nvarchar](100) NULL,
	[created] [date] NULL,
	[deleted] [bit] NULL,
	[deleted_date] [date] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCT]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brand_id] [int] NULL,
	[name] [nvarchar](100) NULL,
	[alias] [nvarchar](100) NULL,
	[image] [nvarchar](100) NULL,
	[price] [float] NULL,
	[stock] [int] NULL,
	[warranty] [int] NULL,
	[gift] [nvarchar](200) NULL,
	[specification] [ntext] NULL,
	[description] [ntext] NULL,
	[created] [datetime] NULL,
	[deleted] [bit] NULL,
	[deleted_date] [date] NULL,
 CONSTRAINT [PK_PRODUCT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SHIPMENT]    Script Date: 29/11/2017 19:11:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHIPMENT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [date] NULL,
	[product_id] [int] NULL,
	[quantity] [int] NULL,
	[supplier] [nvarchar](100) NULL,
 CONSTRAINT [PK_SHIPMENT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201711170346114_InitialCreate', N'DA2.Web.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5C6D6FE33612FE7EC0FD07419FEE0EA99538DDC55E60B7489DE42EE8E605EB6CEFBE2D68897684952855A2D204457FD97DB89FD4BFD0A144C9E29B5E6CC5768A051611397C66381C92C3E1D0BFFFEFFF93EF9FC3C07AC249EA47646A9F8C8E6D0B1337F27CB29ADA195D7EF3C1FEFEBBBFFE6572E985CFD64F25DD29A38396249DDA8F94C6678E93BA8F3844E928F4DD244AA3251DB951E8202F72C6C7C7FF744E4E1C0C10366059D6E45346A81FE2FC033E671171714C3314DC441E0E525E0E35F31CD5BA45214E63E4E2A97D713E1EFD072F4605A56D9D073E0229E63858DA162224A288828C679F533CA7494456F3180A50F0F01263A05BA220C55CF6B33579D76E1C8F59379C75C312CACD521A853D014F4EB95E1CB9F946DAB52BBD81E62E41C3F485F53AD7DED4BEF6705EF4290A400132C3B3599030E2A97D53B1384FE35B4C4765C35101799500DC2F51F27554473CB23AB73BAAEC683C3A66FF8EAC5916D02CC15382339AA0E0C8BACF1681EFFE885F1EA2AF984C4F4F16CBD30FEFDE23EFF4FDB7F8F45DBDA7D057A0130AA0E83E89629C806C7859F5DFB61CB19D2337AC9AD5DA145A015B8229615B37E8F923262BFA089365FCC1B6AEFC67EC9525DCB83E131F661034A249069FB75910A04580AB7AA79127FBBF81EBF8DDFB41B8DEA2277F950FBDC41F264E02F3EA130EF2DAF4D18F8BE9258CF7174E76954421FB16EDABA8FD328FB2C4659D898C240F2859612A4A3771D6C6DBC9A419D4F0665DA21EBE69334955F3D692B20E6D32134A16BB9E0DA5BCAFCBB7B3C59DC7310C5E6E5A4C234D06276E5423A9E591C5EBD72673D2D5640874E5CFBC025E86C80F0658023B7001CF63E92721AE7AF943040687486F99EF519AC20AE0FD1BA58F0DA2C39F03883EC76E968061CE290AE357E776FF18117C9B850B66EFBBE335D8D03CFC125D219746C92561ADB6C6FB18B95FA38C5E12EF0251FC99BA2520FB7CF0C3EE00838873EEBA384DAFC098B1378BC0B12E01AF093D1DF786638BD3BE5D905980FC50EF8348CBE8979274ED87E829145FC440A6F3479A44FD18AD7CD24DD492D42C6A41D12A2A27EB2B2A03EB2629A7340B9A13B4CA59500DE6E1E52334BC8B97C31EBE8FB7DDE66D5A0B6A6A9CC30A89FF85094E6019F3EE11A53821EB11E8B26EECC359C8878F317DF5BD29E7F4130AB2A1596D341BF24560F8D990C31EFE6CC8C584E227DF635E4987834F490CF09DE8F567AAF6392749B6EBE9207473D7CC77B30698A6CB799A46AE9FCF024DC88B072C44F9C187B3DAA317456FE40808740C0CDD675B1E9440DF6CD9A8EEC8050E30C5D6B95B840467287591A7AA113AE4F510ACDC513582AD2321A270FF507882A5E3843542EC1094C24CF50955A7854F5C3F4641AB96A4961DB730D6F78A875C7381634C18C3564D7461AE0F7C30012A3ED2A0B46968E2D42CAED9100D5EAB69CCDB5CD8F5B82BF1889DD8648BEF6CB04BEEBFBD8A61366B6C07C6D9AC922E02188378FB30507E56E96A00F2C1E5D00C543A31190C94BB543B315051637B305051256FCE408B236AD7F197CEAB87669EE24179F7DB7AA3BAF6609B823E0ECC340BDF13DA50688113D53C2F16AC123F53CDE10CE4E4E7B394BBBAB28930F039A662C866EDEF6AFD50A7194436A226C0B5A1B580F2EB3F054899503D842B63798DD2712FA2076C19776B84E56BBF045BB30115BB7E0D5A23345F96CAC6D9E9F451F5ACB206C5C83B1D166A381A8390172FB1E31D94628ACBAA8AE9E20BF7F1866B1DE383D1A0A016CFD5A0A4B233836BA934CD762DE91CB23E2ED9565A92DC278396CACE0CAE256EA3ED4AD238053DDC82AD54246EE1034DB632D251ED3655DDC42932A378C1C431A4504D6E501CFB64554BA9E225D6BCC8A79A7D33EF9F6C1416188E9B6A728E2A692B4E344AD00A4BB5C01A24BDF293945E208A1688C579665EA89069F756C3F25FB2AC6F9FEA2096FB4049CDFE2E5A4897F6C23EAB3A22BCFD15F42E64DE4C1E42D78CBDBEB9C5D2DB5080124DD47E16055948CCCE95B9757177576F5F94A8081347925F719E144D292EAEA8F64E83A24E880106A8F25B361F24338449D5A5D75957B6C91335A39481A93A8A2958B5B7413339309D074AF60BFB8F532BC2EBCC279E8C5207E0453D316AF90C0A58ADAE3BAA987252C7146BBA234A79257548A9AA8794F5EC1141C87AC54678068DEA29BA7350F345EAE86A6D77644DE6481D5A53BD01B64666B9AE3BAA26B9A40EACA9EE8EBDCE349117D003DEB18CA7958DB6ACE230BBDD9E65C0789DD570982DAF76675F07AA15F7C4E2B7F20A182F3F484B329EE836B2A4227EB19D251930CC2B8E70D32D2E388DD7F3664CE1FA5A58D49BAEEFCD78FDECF555AD4239CCC92415F7EA50271DDE26FC20D5FE484639591524B655AA1136F49794E270C40846F39F8359E063B67C97043788F84B9CD22265C31E1F9F8CA5B73687F3EEC549532FD01C444D8F5FC431DB41F615794289FB88123517628BB7216B5025CC7C4D3CFC3CB57FCD5B9DE5110BF6575E7C645DA79F89FF7306150F4986ADDFD4DCCE6172E59B0F5607FAB2A1BB56AFFFFBA5687A64DD253063CEAC6349979B8CB0F8DEA1973445D32DA4D9F815C4DB9D50C253032DAA3421367F59B0F0E920AF0A4A29FF16A2E7BFF7154DFB72602B44CDEB80A1F00651A129FB7F132C63E6BF079F34CFFCEFD759FD4B804D4433BE02F0497F30F90D40F765A86CB9C7AD46731EDAC59294EBB935877AAB84CA7DEF4D4AAAF556135D4DA7EE01B745CAF40696F1C6B28D07DB1D35C9C48361EFD3B45F3D83F8509286D7E91CFBCD15DE657A70C355D09F2A2BF800F2D8347939FBCFFDDDB5AD9962B8079E40D92FC3F7C08C8D676BED3F8F77D7C6660AF31EB8B1F5CAD63D305BDBD7FEB9674BEBBC85EE3DF7564D2332DCC5E862C16DB9B545E01C4EF88B088CA0F0288B2791FA64AEA644D416866B123353731699CC5899380A5F85A2996DBFBEF20DBFB1B39CA699AD21F7B289375FFF1B79739A66DE868CC67D64056B730A7599DA2DEB5853E2D35BCA02167AD29274DEE6B3365EACBFA5A4DF419422CC1EC31DF1DBC9F11D4425434E9D1E39BDEA752FEC9DB55F4E84FD3BF5576B08F63B8A04BBC2AE59D15C9365546EDE9244258914A1B9C11479B0A59E27D45F229742358B31E76FBAF3B81DBBE95860EF9ADC6534CE287419878B4008783127A0897F9EB82CCA3CB98BF39F2719A20B20A6CF62F377E487CC0FBC4AEE2B4D4CC800C1BC0B1ED1656349596477F55221DD46A42310575FE5143DE0300E002CBD2373F48437910DCCEF235E21F7651D013481B40F84A8F6C9858F56090A538EB16E0F9F60C35EF8FCDD1F79A9830B40540000, N'6.1.3-40302')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2fd49e66-0a34-4109-b50f-dda50bede930', N'quyenjino96@gmail.com', 0, N'AMKpr7xfST6FHgKJPoaipuAhK/B4bwv60Sc4+qG2Ly7e32HwB9Xpanas9mYwmKheHg==', N'b6f1cb9f-1b62-4076-bda4-2884649deb1f', NULL, 0, 0, NULL, 1, 0, N'quyenjino96@gmail.com')
SET IDENTITY_INSERT [dbo].[BILL] ON 

INSERT [dbo].[BILL] ([id], [status], [created], [customer_id], [amount], [nguoi_nhan], [dia_chi], [so_dt]) VALUES (1, 0, CAST(0x0000A83900A870FA AS DateTime), 1, 40980000, N'TVQ', N'PM1401', N'01222292364')
INSERT [dbo].[BILL] ([id], [status], [created], [customer_id], [amount], [nguoi_nhan], [dia_chi], [so_dt]) VALUES (2, 0, CAST(0x0000A83900DADB3A AS DateTime), 1, 50670000, N'TVQ', N'PM1401', N'01222292364')
INSERT [dbo].[BILL] ([id], [status], [created], [customer_id], [amount], [nguoi_nhan], [dia_chi], [so_dt]) VALUES (1002, 0, CAST(0x0000A83A00F17851 AS DateTime), 1, 111060000, N'TVQ', N'PM1401', N'01222292364')
SET IDENTITY_INSERT [dbo].[BILL] OFF
SET IDENTITY_INSERT [dbo].[BILLITEM] ON 

INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (1, 1, 10, 2, 20490000)
INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (2, 2, 9, 2, 12990000)
INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (3, 2, 13, 1, 24690000)
INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (1002, 1002, 10, 4, 20490000)
INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (1003, 1002, 12, 2, 13490000)
INSERT [dbo].[BILLITEM] ([id], [bill_id], [product_id], [quantity], [price]) VALUES (1004, 1002, 15, 1, 2120000)
SET IDENTITY_INSERT [dbo].[BILLITEM] OFF
SET IDENTITY_INSERT [dbo].[BRAND] ON 

INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (1, N'Laptop Asus', N'laptop-asus', 1, NULL, NULL)
INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (2, N'Laptop HP', N'laptop-hp', 1, NULL, NULL)
INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (3, N'Laptop Dell', N'laptop-dell', 1, NULL, NULL)
INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (4, N'PC Asus', N'pc-asus', 2, NULL, NULL)
INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (5, N'PC Lenovo', N'pc-lenovo', 2, NULL, NULL)
INSERT [dbo].[BRAND] ([id], [name], [alias], [category_id], [deleted], [deleted_date]) VALUES (6, N'Soundmax', N'pk-soundmax', 3, NULL, NULL)
SET IDENTITY_INSERT [dbo].[BRAND] OFF
SET IDENTITY_INSERT [dbo].[CATEGORY] ON 

INSERT [dbo].[CATEGORY] ([id], [name], [alias], [deleted], [deleted_date]) VALUES (1, N'Laptop', N'laptop', NULL, NULL)
INSERT [dbo].[CATEGORY] ([id], [name], [alias], [deleted], [deleted_date]) VALUES (2, N'PC', N'pc', NULL, NULL)
INSERT [dbo].[CATEGORY] ([id], [name], [alias], [deleted], [deleted_date]) VALUES (3, N'Phụ kiện', N'phu-kien', NULL, NULL)
SET IDENTITY_INSERT [dbo].[CATEGORY] OFF
SET IDENTITY_INSERT [dbo].[CUSTOMER] ON 

INSERT [dbo].[CUSTOMER] ([id], [name], [email], [password], [tel], [address], [created], [deleted], [deleted_date]) VALUES (1, N'TVQ', N'quyen.tvq96@gmail.com', N'123456', N'01222292364', N'PM1401', CAST(0x933D0B00 AS Date), NULL, NULL)
SET IDENTITY_INSERT [dbo].[CUSTOMER] OFF
SET IDENTITY_INSERT [dbo].[PRODUCT] ON 

INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (9, 1, N'Laptop Asus S410UA-EB218T (I3-7100U)', N'laptop-Asus-S410UA-EB218T-I3-7100U', N'14558_1510971693-7.jpg', 12990000, 100, 24, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/May-xach-tay-Laptop-Asus-S410UA-EB218T-I3-7100U', NULL, CAST(0x0000A83900000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (10, 1, N'Laptop Asus S410UA-EB220T (I7-8550U)', N'laptop-Asus-S410UA-EB220T-I7-8550U', N'14554_1510974943-7.jpg', 20490000, 50, 24, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/May-xach-tay-Laptop-Asus-S410UA-EB220T-I7-8550U', NULL, CAST(0x0000A83900000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (11, 1, N'Laptop Asus S410UA-EB015T (I5-8250U)', N'laptop-Asus-S410UA-EB015T-I5-8250U', N'14556_1510973893-1.jpg', 17490000, 20, 24, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/may-tinh/may-tinh-xach-tay-laptop/asus/May-xach-tay-Laptop-Asus-S410UA-EB015T-I5-8250U', NULL, CAST(0x0000A83A00000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (12, 2, N'Laptop HP Pavilion X360 14-ba063TU (2GV25PA) (Vàng)', N'laptop-hp-pavilion-x360-14-ba063tu-2gv25pa-vang', N'14420_1510214554-1.jpg', 13490000, 10, 12, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/may-xach-tay-laptop-hp-pavilion-x360-14-ba063tu-2gv25pa-vang', NULL, CAST(0x0000A83B00000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (13, 2, N'Laptop HP Probook 450 G5-2XR67PA', N'laptop-HP-Probook-450-G5-2XR67PA', N'14386_1510022941-1.jpg', 24690000, 0, 12, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/May-xach-tay-Laptop-HP-Probook-450-G5-2XR67PA', NULL, CAST(0x0000A83A00000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (14, 4, N'Máy tính để bàn/ PC Asus UN62-M218M', N'may-tinh-de-ban-pc-asus-un62-m218m', N'14538_1510885124-1.jpg', 6490000, 12, 24, N'Tặng kèm 1 Thẻ cào Mobi 500.000', N'https://phongvu.vn/may-tinh-de-ban-pc-asus-un62-m218m', NULL, CAST(0x0000A83900000000 AS DateTime), 0, NULL)
INSERT [dbo].[PRODUCT] ([id], [brand_id], [name], [alias], [image], [price], [stock], [warranty], [gift], [specification], [description], [created], [deleted], [deleted_date]) VALUES (15, 6, N'Loa Soundmax AK 800', N'loa-Soundmax-AK-800', N'14350_1509673606-1.png', 2120000, 20, 2, NULL, N'https://phongvu.vn/thiet-bi-tin-hoc/speaker-loa/Loa-Soundmax-AK-800', NULL, CAST(0x0000A83800000000 AS DateTime), 0, NULL)
SET IDENTITY_INSERT [dbo].[PRODUCT] OFF

go
-- trigger cap nhat so luong
create trigger auto_minus_quantity
on BILL
after insert, update
as
declare @status int;
declare @product_id int;
declare @quantity int;
declare @bill_id int;

select @status = inserted.status
from inserted

if (@status = 2)
begin
	declare @billItem_id int;
	select @bill_id = inserted.id from inserted
	declare cursor_updateQuantity cursor
	dynamic 
	for select id from BILLITEM where bill_id = @bill_id 
	open cursor_updateQuantity
	fetch next from cursor_updateQuantity into @billItem_id
	while @@FETCH_STATUS = 0
	begin
		select @quantity = quantity, @product_id = product_id
		from BILLITEM
		where id = @billItem_id
			
		update PRODUCT
		set stock = stock - @quantity
		where id = @product_id

		fetch next from cursor_updateQuantity into @billItem_id
	end
end

