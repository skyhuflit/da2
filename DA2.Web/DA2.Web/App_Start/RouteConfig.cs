﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DA2.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "AddCart",
                url: "them-gio-hang",
                defaults: new { controller = "Cart", action = "AddItem", id = UrlParameter.Optional },
                namespaces: new[] { "DA2.Web.Controllers" }
            );

            routes.MapRoute(
                name: "DanhMuc",
                url: "danh-muc/{alias}",
                defaults: new { controller = "DanhMuc", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "DA2.Web.Controllers" }
            );

            routes.MapRoute(
                name: "SanPham",
                url: "san-pham/{aliasSP}",
                defaults: new { controller = "SanPham", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "DA2.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
