﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DA2.Web.Common
{
    public class CommonConstant
    {
        public static string CustomerSession { get; set; }
        public const string CartSession = "CartSession";
    }
}