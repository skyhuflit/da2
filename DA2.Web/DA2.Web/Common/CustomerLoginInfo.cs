﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DA2.Web.Common
{
    [Serializable]
    public class CustomerLoginInfo
    {
        public int id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string tel { get; set; }
        public string address { get; set; }
    }
}