﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DA2.Web.Models
{
    public class RegisterModel
    {
        [Key]
        public int id { get; set; }

        [Display(Name="Email")]
        [Required(ErrorMessage="Vui lòng nhập email")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [StringLength(20, MinimumLength=6, ErrorMessage="Độ dài mật khẩu ít nhất 6 kí tự")]
        public string password { get; set; }

        [Display(Name = "Xác nhận mật khẩu")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage="Mật khẩu không trùng khớp")]
        public string confirmPassword { get; set; }

        [Display(Name = "Họ tên")]
        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        public string name { get; set; }

        [Display(Name = "Điện thoại")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        public string tel { get; set; }

        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "Vui lòng nhập địa chỉ")]
        public string address { get; set; }
    }
}