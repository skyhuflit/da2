﻿using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DA2.Web.Models
{
    [Serializable]
    public class CartItem
    {
        public PRODUCT Product { get; set; }
        public int Quantity { get; set; }
    }
}