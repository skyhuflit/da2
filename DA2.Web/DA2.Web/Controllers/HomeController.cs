﻿using DA2.Data.Infrastructure;
using DA2.Web.Common;
using DA2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();
        public ActionResult Index()
        {
            var newProducts = _uow.ProductRepository.Get(orderBy: q => q.OrderBy(d => d.created)).Where(x => x.deleted == false).Skip(1).Take(6);
            ViewBag.newest = _uow.ProductRepository.NewestProduct();

           
            return View(newProducts);
        }
    }
}