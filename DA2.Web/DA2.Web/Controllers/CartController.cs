﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using DA2.Web.Common;
using DA2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DA2.Web.Controllers
{
    public class CartController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();

        // GET: Cart
        public ActionResult Index()
        {
            var cart = Session[CommonConstant.CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        [ChildActionOnly]
        public PartialViewResult CartDropdown()
        {
            var cart = Session[CommonConstant.CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return PartialView(list);
        }

        public ActionResult AddItem(int productId, int quantity)
        {
            var sp = _uow.ProductRepository.GetById(productId);

            var cart = Session[CommonConstant.CartSession];
            if (cart != null)
            {
                var list = (List<CartItem>)cart; //ép kiểu vì biết thừa đó là list

                if (list.Exists(x => x.Product.id == productId)) //nếu giỏ đã có sản phẩm
                {
                    foreach (var item in list)
                    {
                        if (item.Product.id == productId)
                        {
                            item.Quantity += quantity;
                        }
                    }
                }
                else //chưa có
                {
                    //tạo mới đối tượng
                    var item = new CartItem() { Product = sp, Quantity = quantity };
                    list.Add(item);
                }
                //gán vào session (ko phải biến cart)
                Session[CommonConstant.CartSession] = list;
            }
            else
            {
                //tạo mới đối tượng
                var item = new CartItem() { Product = sp, Quantity = quantity };
                var list = new List<CartItem>();
                list.Add(item);

                //gán vào session (ko phải biến cart)
                Session[CommonConstant.CartSession] = list;
            }

            return RedirectToAction("Index");
        }

        //https://youtu.be/EAMryhdPWYA
        public JsonResult Update(string cartModel)
        {
            var jsonCart = new JavaScriptSerializer().Deserialize<List<CartItem>>(cartModel);
            var sessionCart = (List<CartItem>)Session[CommonConstant.CartSession];
            if (sessionCart != null)
            {
                foreach (var item in sessionCart)
                {
                    var jsonItem = jsonCart.SingleOrDefault(x => x.Product.id == item.Product.id);
                    if (jsonItem != null)
                    {
                        item.Quantity = jsonItem.Quantity;
                    }
                }
                Session[CommonConstant.CartSession] = sessionCart;
                return Json(new { status = true });
            }
            return Json(new { status = false });
        }

        public JsonResult Delete(int id)
        {
            var sessionCart = (List<CartItem>)Session[CommonConstant.CartSession];
            sessionCart.RemoveAll(x => x.Product.id == id);
            Session[CommonConstant.CartSession] = sessionCart;
            return Json(new { status = true });
        }

        [HttpGet]
        public ActionResult Payment()
        {
            if (Session[CommonConstant.CustomerSession] == null)
            {
                return Redirect("/Auth");
            }
            else if (Session[CommonConstant.CartSession] == null)
            {
                return Redirect("/Cart");
            }
            var cart = Session[CommonConstant.CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        [HttpPost]
        public ActionResult Payment(string nguoi_nhan, string dia_chi, string so_dt)
        {
            var cart = (List<CartItem>)Session[CommonConstant.CartSession];
            var bill = new BILL();
            bill.customer_id = ((CustomerLoginInfo)Session[CommonConstant.CustomerSession]).id; //ép kiểu session để lấy được MaKH
            bill.status = 0;
            bill.created = DateTime.Now;
            bill.amount = cart.Sum(x => x.Product.price * x.Quantity);
            bill.nguoi_nhan = nguoi_nhan;
            bill.dia_chi = dia_chi;
            bill.so_dt = so_dt;

            try
            {
                var newBillId = _uow.BillRepository.InsertBill(bill);
                foreach (var item in cart)
                {
                    var billitem = new BILLITEM();
                    billitem.bill_id = newBillId;
                    billitem.product_id = item.Product.id;
                    billitem.price = item.Product.price;
                    billitem.quantity = item.Quantity;
                    _uow.BillItemRepository.Insert(billitem);
                }
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return Redirect("/Cart/Success");
        }

        public ActionResult Success()
        {
            Session[CommonConstant.CartSession] = null;
            return View();
        }
    }
}