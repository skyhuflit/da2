﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using DA2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Controllers
{
    public class AuthController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();
        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        [HttpGet]
        public PartialViewResult Login()
        {
            return PartialView("Login", new LoginModel());
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if(ModelState.IsValid)
            {
                var result = CheckLogin(model.emailLogin, model.passwordLogin);
                if(result == 1)
                {
                    //thành công: tìm thông tin tài khoản đó và gán vào session
                    var cus = _uow.CustomerRepository.GetAll().Where(x => x.email == model.emailLogin).First();
                    var cusSession = new Common.CustomerLoginInfo();
                    cusSession.id = cus.id;
                    cusSession.email = model.emailLogin;
                    cusSession.name = cus.name;
                    cusSession.tel = cus.tel;
                    cusSession.address = cus.address;
                    Session.Add(Common.CommonConstant.CustomerSession, cusSession);
                    return RedirectToAction("Index", "Home");
                }
                else if(result == 0)
                {
                    ModelState.AddModelError("loginError", "Tài khoản không tồn tại");
                }
                else
                {
                    ModelState.AddModelError("loginError", "Sai mật khẩu");
                }
            }
            return View("Index");
        }

        public int CheckLogin(string email, string password)
        {
            var result = _uow.CustomerRepository.GetAll().Where(x => x.email == email).SingleOrDefault();
            if (result == null)
            {
                return 0; //ko tồn tại
            }
            else
            {
                if (result.password == password)
                {
                    return 1; //ok
                }
                else
                {
                    return -1; //sai pass
                }
            }
        }

        [ChildActionOnly]
        [HttpGet]
        public PartialViewResult Register()
        {
            return PartialView("Register", new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                var checkEmail = _uow.CustomerRepository.GetAll().Where(x => x.email == model.email).FirstOrDefault();
                if(checkEmail != null)
                {
                    ModelState.AddModelError("", "Email này đã tồn tại trong hệ thống cửa hàng");
                }
                else
                {
                    var cus = new CUSTOMER();
                    cus.email = model.email;
                    cus.password = model.password;
                    cus.name = model.name;
                    cus.tel = model.tel;
                    cus.address = model.address;
                    cus.created = DateTime.Now.Date;
                    _uow.CustomerRepository.Insert(cus);
                    _uow.Save();
                    ViewBag.Success = "<< Đăng kí tài khoản thành công. Mời đăng nhập!";
                    ModelState.Clear();
                }
            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            Session[Common.CommonConstant.CustomerSession] = null;
            return Redirect("/");
        }
    }
}