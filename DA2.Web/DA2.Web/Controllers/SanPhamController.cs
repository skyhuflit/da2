﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Controllers
{
    public class SanPhamController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();
        public ActionResult Index(string aliasSP)
        {
            PRODUCT sp = _uow.ProductRepository.Get(x => x.alias.Equals(aliasSP)).Where(x => x.deleted == false).SingleOrDefault();
            ViewBag.Random = _uow.ProductRepository.GetAll().Where(x => x.deleted == false).Take(10);
            return View(sp);
        }
    }
}