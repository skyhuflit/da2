﻿using DA2.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace DA2.Web.Controllers
{
    public class DanhMucController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();
        private static string alias { get; set; }
        public ActionResult Index(string alias)
        {
            ViewBag.alias = alias;
            DanhMucController.alias = alias;
            return View();
        }

        public PartialViewResult GetPaging(int? page)
        {
            var list = _uow.ProductRepository.GetAll().Where(x => x.BRAND.alias.Equals(DanhMucController.alias)).Where(x => x.deleted == false).OrderBy(x => x.created);
            int pageSize = 9;
            int pageNumber = (page ?? 1);
            if (list.ToList().Count == 0)
                return PartialView("_404");
            else
                return PartialView("_PartialViewDMSP", list.ToPagedList(pageNumber, pageSize));
        }
    }
}