﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using DA2.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Controllers
{
    public class TaiKhoanController : Controller
    {
        private UnitOfWork _uow = new UnitOfWork();
        // GET: TaiKhoan
        public ActionResult Index()
        {
            if (Session[CommonConstant.CustomerSession] == null)
            {
                return Redirect("/Auth");
            }
            var cusId = ((CustomerLoginInfo)Session[CommonConstant.CustomerSession]).id;
            var cusInfo = _uow.CustomerRepository.GetById(cusId);
            return View(cusInfo);
        }

        [HttpPost]
        public ActionResult Index(CUSTOMER cus)
        {
            if(ModelState.IsValid)
            {
                var updateCus = _uow.CustomerRepository.GetById(cus.id);
                updateCus.name = cus.name;
                updateCus.tel = cus.tel;
                updateCus.address = cus.address;
                updateCus.password = updateCus.password;
                _uow.CustomerRepository.Update(updateCus);
                _uow.Save();
                ViewBag.Success = "Cập nhật thông tin thành công!";
            }
            return View("Index");
        }

        public ActionResult LichSuDatHang()
        {
            if (Session[CommonConstant.CustomerSession] == null)
            {
                return Redirect("/Auth");
            }
            var cusId = ((CustomerLoginInfo)Session[CommonConstant.CustomerSession]).id;
            var cusBill = _uow.BillRepository.GetAll().Where(x => x.customer_id == cusId);
            return View(cusBill);
        }

        public PartialViewResult XemDonHang(int id)
        {
            var billitem = _uow.BillItemRepository.GetAll().Where(x => x.bill_id == id);
            ViewBag.id = id;
            ViewBag.bill = _uow.BillRepository.GetById(id);
            return PartialView("XemDonHang", billitem);
        }

        [HttpGet]
        public ActionResult DoiMatKhau()
        {
            return View();

        }
        
    }
}