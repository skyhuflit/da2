﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DA2.Web.Startup))]
namespace DA2.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
