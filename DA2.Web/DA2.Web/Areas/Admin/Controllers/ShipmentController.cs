﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class ShipmentController : BaseController
    {
        UnitOfWork _uow = new UnitOfWork();

        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        // GET: Admin/Shipment
        public ActionResult Index()
        {
            return View(_uow.ShipmentRepository.GetAll().ToList());
        }

        // GET: Admin/Shipment/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name");
            return View();
        }

        // POST: Admin/Shipment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,date,product_id,quantity,supplier")] SHIPMENT sHIPMENT)
        {
            if (ModelState.IsValid)
            {
                _uow.ShipmentRepository.Ship(sHIPMENT);
                Success(string.Format("Sửa thành công <b>{0}</b>", sHIPMENT.id));
                return RedirectToAction("Index");
            }

            Danger("Có gì đó sai sai, đã xảy ra lỗi");
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name", sHIPMENT.product_id);
            return View(sHIPMENT);
        }

        //// GET: Admin/Shipment/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SHIPMENT sHIPMENT = db.SHIPMENTs.Find(id);
        //    if (sHIPMENT == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.product_id = new SelectList(db.PRODUCTs, "id", "name", sHIPMENT.product_id);
        //    return View(sHIPMENT);
        //}

        // POST: Admin/Shipment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "id,date,product_id,quantity,supplier")] SHIPMENT sHIPMENT)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(sHIPMENT).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.product_id = new SelectList(db.PRODUCTs, "id", "name", sHIPMENT.product_id);
        //    return View(sHIPMENT);
        //}

        // GET: Admin/Shipment/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SHIPMENT sHIPMENT = db.SHIPMENTs.Find(id);
        //    if (sHIPMENT == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(sHIPMENT);
        //}

        // POST: Admin/Shipment/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    SHIPMENT sHIPMENT = db.SHIPMENTs.Find(id);
        //    db.SHIPMENTs.Remove(sHIPMENT);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
