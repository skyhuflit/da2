﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class BrandController : BaseController
    {
        private UnitOfWork _uow = new UnitOfWork();

        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        // GET: Admin/Brand
        public ActionResult Index()
        {
            var bRANDs = _uow.BrandRepository.GetAll();
            return View(bRANDs.ToList());
        }

        // GET: Admin/Brand/Details/5
        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BRAND bRAND = _uow.BrandRepository.GetById(id);
            if (bRAND == null)
            {
                return HttpNotFound();
            }
            return View(bRAND);
        }

        // GET: Admin/Brand/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            ViewBag.category_id = new SelectList(_uow.CategoryRepository.GetAll(), "id", "name");
            return View();
        }

        // POST: Admin/Brand/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,alias,category_id,deleted,deleted_date")] BRAND bRAND)
        {
            if (ModelState.IsValid)
            {
                _uow.BrandRepository.Insert(bRAND);
                _uow.Save();

                Success(string.Format("Thêm thành công <b>{0}</b>", bRAND.name));
                return RedirectToAction("Index");
            }

            ViewBag.category_id = new SelectList(_uow.CategoryRepository.GetAll(), "id", "name", bRAND.category_id);

            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(bRAND);
        }

        // GET: Admin/Brand/Edit/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BRAND bRAND = _uow.BrandRepository.GetById(id);
            if (bRAND == null)
            {
                return HttpNotFound();
            }
            ViewBag.category_id = new SelectList(_uow.CategoryRepository.GetAll(), "id", "name", bRAND.category_id);
            return View(bRAND);
        }

        // POST: Admin/Brand/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,alias,category_id,deleted,deleted_date")] BRAND bRAND)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(bRAND).State = EntityState.Modified;
                _uow.BrandRepository.Update(bRAND);
                _uow.Save();
                Success(string.Format("Cập nhật thành công <b>{0}</b>", bRAND.name));
                return RedirectToAction("Index");
            }
            ViewBag.category_id = new SelectList(_uow.CategoryRepository.GetAll(), "id", "name", bRAND.category_id);
            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(bRAND);
        }

        // GET: Admin/Brand/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BRAND bRAND = _uow.BrandRepository.GetById(id);
            if (bRAND == null)
            {
                return HttpNotFound();
            }
            return View(bRAND);
        }

        // POST: Admin/Brand/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BRAND bRAND = _uow.BrandRepository.GetById(id);
            _uow.BrandRepository.DeleteBrand(bRAND);
            _uow.Save();
            Success(string.Format("Xóa thành công hãng có id là <b>{0}</b>", id));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
