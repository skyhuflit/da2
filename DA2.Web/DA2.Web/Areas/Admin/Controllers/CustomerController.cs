﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class CustomerController : BaseController
    {
        UnitOfWork _uow = new UnitOfWork();

        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        // GET: Admin/Customer
        public ActionResult Index()
        {
            return View(_uow.CustomerRepository.GetAll());
        }


        // GET: Admin/Customer/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,email,password,tel,address,created,deleted,deleted_date")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                cUSTOMER.created = DateTime.Now.Date;
                _uow.CustomerRepository.Insert(cUSTOMER);
                _uow.Save();

                Success(string.Format("Thêm thành công <b>{0}</b>", cUSTOMER.name));
                return RedirectToAction("Index");
            }
            Danger("Có gì đó sai sai, đã xảy ra lỗi!");

            return View(cUSTOMER);
        }

        // GET: Admin/Customer/Edit/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = _uow.CustomerRepository.GetById(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: Admin/Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,email,password,tel,address,created,deleted,deleted_date")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                _uow.CustomerRepository.Update(cUSTOMER);
                _uow.Save();
                Success(string.Format("Sửa thành công <b>{0}</b>", cUSTOMER.name));
                return RedirectToAction("Index");
            }
            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(cUSTOMER);
        }

        // GET: Admin/Customer/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = _uow.CustomerRepository.GetById(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: Admin/Customer/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CUSTOMER cUSTOMER = _uow.CustomerRepository.GetById(id);
            _uow.CustomerRepository.DeleteCustomer(cUSTOMER);
            _uow.Save();
            Success(string.Format("Xóa thành công <b>{0}</b>", cUSTOMER.name));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
