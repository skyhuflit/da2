﻿using DA2.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        public void Success(string message)
        {
            AddAlert(AlertStyles.Success, message);
        }

        public void Information(string message)
        {
            AddAlert(AlertStyles.Information, message);
        }

        public void Warning(string message)
        {
            AddAlert(AlertStyles.Warning, message);
        }

        public void Danger(string message)
        {
            AddAlert(AlertStyles.Danger, message);
        }

        private void AddAlert(string alertStyle, string message)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message
            });

            TempData[Alert.TempDataKey] = alerts;
        }
    }
}