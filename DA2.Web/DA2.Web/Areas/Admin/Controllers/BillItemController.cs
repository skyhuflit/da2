﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class BillItemController : Controller
    {
        UnitOfWork _uow = new UnitOfWork();

        // GET: Admin/BillItem
        public ActionResult Index(int? id)
        {
           
            return View(_uow.BillItemRepository.GetBillItemWithID(id));
        }

        // GET: Admin/BillItem/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BILLITEM bILLITEM = _uow.BillItemRepository.GetById(id);
            if (bILLITEM == null)
            {
                return HttpNotFound();
            }
            return View(bILLITEM);
        }

        // GET: Admin/BillItem/Create
        public ActionResult Create()
        {
            ViewBag.bill_id = new SelectList(_uow.BillRepository.GetAll(), "id", "nguoi_nhan");
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name");
            return View();
        }

        // POST: Admin/BillItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,bill_id,product_id,quantity,price")] BILLITEM bILLITEM)
        {
            if (ModelState.IsValid)
            {
                _uow.BillItemRepository.Insert(bILLITEM);
                _uow.Save();
                return RedirectToAction("Index");
            }

            ViewBag.bill_id = new SelectList(_uow.BillRepository.GetAll(), "id", "nguoi_nhan", bILLITEM.bill_id);
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name", bILLITEM.product_id);
            return View(bILLITEM);
        }

        // GET: Admin/BillItem/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BILLITEM bILLITEM = _uow.BillItemRepository.GetById(id);
            if (bILLITEM == null)
            {
                return HttpNotFound();
            }
            ViewBag.bill_id = new SelectList(_uow.BillRepository.GetAll(), "id", "nguoi_nhan", bILLITEM.bill_id);
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name", bILLITEM.product_id);
            return View(bILLITEM);
        }

        // POST: Admin/BillItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,bill_id,product_id,quantity,price")] BILLITEM bILLITEM)
        {
            if (ModelState.IsValid)
            {
                _uow.BillItemRepository.Update(bILLITEM);
                _uow.Save();
                return RedirectToAction("Index");
            }
            ViewBag.bill_id = new SelectList(_uow.BillRepository.GetAll(), "id", "nguoi_nhan", bILLITEM.bill_id);
            ViewBag.product_id = new SelectList(_uow.ProductRepository.GetAllActiveProduct(), "id", "name", bILLITEM.product_id);
            return View(bILLITEM);
        }

        // GET: Admin/BillItem/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BILLITEM bILLITEM = _uow.BillItemRepository.GetById(id);
            if (bILLITEM == null)
            {
                return HttpNotFound();
            }
            return View(bILLITEM);
        }

        // POST: Admin/BillItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BILLITEM bILLITEM = _uow.BillItemRepository.GetById(id);
            _uow.BillItemRepository.Delete(bILLITEM);
            _uow.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
