﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        private DA2DbContext db = new DA2DbContext();

        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        // GET: Admin/Category
        public ActionResult Index()
        {
            return View(db.CATEGORies.ToList());
        }

        // GET: Admin/Category/Details/5
        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATEGORY cATEGORY = db.CATEGORies.Find(id);
            if (cATEGORY == null)
            {
                return HttpNotFound();
            }
            return View(cATEGORY);
        }

        // GET: Admin/Category/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,alias,deleted,deleted_date")] CATEGORY cATEGORY)
        {
            if (ModelState.IsValid)
            {
                db.CATEGORies.Add(cATEGORY);
                db.SaveChanges();
                Success(string.Format("Thêm thành công <b>{0}</b>", cATEGORY.id));
                return RedirectToAction("Index");
            }

            Danger("Có gì đó sai sai, đã xảy ra lỗi!");

            return View(cATEGORY);
        }

        // GET: Admin/Category/Edit/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATEGORY cATEGORY = db.CATEGORies.Find(id);
            if (cATEGORY == null)
            {
                return HttpNotFound();
            }
            return View(cATEGORY);
        }

        // POST: Admin/Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,alias,deleted,deleted_date")] CATEGORY cATEGORY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cATEGORY).State = EntityState.Modified;
                db.SaveChanges();
                Success(string.Format("Sửa thành công <b>{0}</b>", cATEGORY.id));
                return RedirectToAction("Index");
            }

            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(cATEGORY);
        }

        // GET: Admin/Category/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATEGORY cATEGORY = db.CATEGORies.Find(id);
            if (cATEGORY == null)
            {
                return HttpNotFound();
            }
            return View(cATEGORY);
        }

        // POST: Admin/Category/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CATEGORY cATEGORY = db.CATEGORies.Find(id);
            db.CATEGORies.Remove(cATEGORY);
            db.SaveChanges();
            Success(string.Format("Xóa thành công <b>{0}</b>", cATEGORY.id));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
