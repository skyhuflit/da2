﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DA2.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
    public class DashboardController : Controller
    {
        UnitOfWork _uow = new UnitOfWork();
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            int activeProductCount = _uow.ProductRepository.GetAllActiveProduct().Count();
            ViewBag.ActiveProduct = activeProductCount;

            List<PRODUCT> productList = _uow.ProductRepository.GetAll().ToList();
            ViewBag.AllProduct = productList;

            int customerCount = _uow.CustomerRepository.GetAll().Count();
            ViewBag.CustomerCount = customerCount;

            List<BRAND> brandList = _uow.BrandRepository.GetAll().ToList();
            ViewBag.AllBrand = brandList;

            double? totalIncome = 0;
            List<BILL> billList = _uow.BillRepository.GetConfirmedBill();
            for (int i = 0; i < billList.Count; i++)
            {
                totalIncome += billList[i].amount;
            }

            ViewBag.TotalIncome = totalIncome;

            ViewBag.SoldAmount = _uow.BillItemRepository.GetSoldItemAmount();

            return View();
        }
    }
}