﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class BillController : BaseController
    {
        UnitOfWork _uow = new UnitOfWork();

        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        // GET: Admin/Bill
        public ActionResult Index()
        {
            return View(_uow.BillRepository.GetAll().ToList());
        }

        // GET: Admin/Bill/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            ViewBag.customer_id = new SelectList(_uow.CustomerRepository.GetAllActiveCustomer(), "id", "name");
            return View();
        }

        // POST: Admin/Bill/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,status,created,customer_id,amount,nguoi_nhan,dia_chi,so_dt")] BILL bILL)
        {
            if (ModelState.IsValid)
            {
                _uow.BillRepository.Insert(bILL);
                _uow.Save();
                return RedirectToAction("Index");
            }

            ViewBag.customer_id = new SelectList(_uow.CustomerRepository.GetAllActiveCustomer(), "id", "name", bILL.customer_id);
            return View(bILL);
        }

        // GET: Admin/Bill/Edit/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BILL bILL = _uow.BillRepository.GetById(id);
            if (bILL == null)
            {
                return HttpNotFound();
            }
            ViewBag.customer_id = new SelectList(_uow.CustomerRepository.GetAllActiveCustomer(), "id", "name", bILL.customer_id);
            return View(bILL);
        }

        // POST: Admin/Bill/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,status,created,customer_id,amount,nguoi_nhan,dia_chi,so_dt")] BILL bILL)
        {
            if (ModelState.IsValid)
            {
                _uow.BillRepository.Update(bILL);
                _uow.Save();
                return RedirectToAction("Index");
            }
            ViewBag.customer_id = new SelectList(_uow.CustomerRepository.GetAllActiveCustomer(), "id", "name", bILL.customer_id);
            return View(bILL);
        }

        // GET: Admin/Bill/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BILL bILL = _uow.BillRepository.GetById(id);
            if (bILL == null)
            {
                return HttpNotFound();
            }
            return View(bILL);
        }

        // POST: Admin/Bill/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost, ActionName("Confirm")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmConfirmed(int id, FormCollection form)
        {
            BILL bill = _uow.BillRepository.GetById(id);
            int status = Int16.Parse(form["status"]);
            _uow.BillRepository.ConfirmBill(bill, status);
            _uow.Save();
            Success(string.Format("Cập nhật thành công <b>{0}</b>", bill.id));

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
