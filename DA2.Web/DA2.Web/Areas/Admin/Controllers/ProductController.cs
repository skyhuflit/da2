﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DA2.Data.Model;
using DA2.Data.Infrastructure;
using System.IO;

namespace DA2.Web.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        private UnitOfWork _uow = new UnitOfWork();


        // GET: Admin/Product
        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        public ActionResult Index()
        {
            var products = _uow.ProductRepository.GetAll();
            return View(products.ToList());
        }

        // GET: Admin/Product/Details/5
        [Authorize(Roles = "Admin, Quản lý, Nhân viên")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT product = _uow.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Admin/Product/Create
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Create()
        {
            ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name");
            return View();
        }

        // POST: Admin/Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,brand_id,name,alias,image,price,stock,warranty,gift,specification,description,created,deleted,deleted_date")] PRODUCT product,
            HttpPostedFileBase fileUp)
        {
            var allowedExtensions = new[] { ".jpg", ".jpeg" };
            product.created = DateTime.Now.Date;
            product.deleted = false;
            if (fileUp == null)
            {
                ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
                Danger("Xin hãy thêm hình ảnh!");
                return View(product);
            }
            if (ModelState.IsValid)
            {
                for (int b = 0; b < Request.Files.Count; b++)
                {
                    var file = Request.Files[b];
                    var fileup = Path.GetFileName(fileUp.FileName);
                    var path = Path.Combine(Server.MapPath("../../ProductImages"), fileup);
                    var ext = Path.GetExtension(fileUp.FileName);
                    if (allowedExtensions.Contains(ext))
                    {
                        if (System.IO.File.Exists(path))
                        {
                            ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
                            Danger("Hình ảnh đã tồn tại");
                            return View(product);
                        }
                        else
                        {
                            fileUp.SaveAs(path);
                        }
                        product.image = fileup;
                        _uow.ProductRepository.Insert(product);
                        _uow.Save();
                        Success(string.Format("Thêm thành công <b>{0}</b>", product.name));
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
                        Danger("Xin hãy chọn hình có đuôi .jpg hoặc .jpeg");
                        return View(product);
                    }
                }
            }

            ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(product);
        }

        // GET: Admin/Product/Edit/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT product = _uow.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
            return View(product);
        }

        // POST: Admin/Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,brand_id,name,alias,image,price,stock,warranty,gift,specification,description,created,deleted,deleted_date")] PRODUCT product)
        {
            if (ModelState.IsValid)
            {
                _uow.ProductRepository.Update(product);
                _uow.Save();
                Success(string.Format("Cập nhật thành công <b>{0}</b>", product.name));
                return RedirectToAction("Index");
            }
            ViewBag.brand_id = new SelectList(_uow.BrandRepository.GetAll(), "id", "name", product.brand_id);
            Danger("Có gì đó sai sai, đã xảy ra lỗi!");
            return View(product);
        }

        // GET: Admin/Product/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT product = _uow.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Admin/Product/Delete/5
        [Authorize(Roles = "Admin, Nhân viên")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _uow.ProductRepository.DeleteProduct(id);
            _uow.Save();
            Success(string.Format("Xóa thành công sản phẩm có id là <b>{0}</b>", id));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _uow.Dispose();
            base.Dispose(disposing);
        }
    }
}
