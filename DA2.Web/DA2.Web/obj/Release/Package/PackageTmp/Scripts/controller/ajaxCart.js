﻿var cart = {
    init: function () {
        cart.regEvents();
    },
    regEvents: function () {
        $('#btnUpdate').off('click').on('click', function (e) {
            e.preventDefault();
            var listProduct = $('.qty');
            var cartList = [];
            $.each(listProduct, function (i, item) {
                cartList.push({
                    Quantity: $(item).val(),
                    Product: {
                        id: $(item).data('id'),
                    }
                });
            });

            $.ajax({
                url: '/Cart/Update',
                data: { cartModel: JSON.stringify(cartList) },
                dataType: 'json',
                type: 'POST',
                success: function (dataBack) {
                    if (dataBack.status == true) {
                        window.location.href = "/Cart";
                    }
                }
            });
        }); //end btn update

        $('.remove').off('click').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/Cart/Delete',
                data: { id: $(this).data('id') }, //data-id
                dataType: 'json',
                type: 'POST',
                success: function (dataBack) {
                    if (dataBack.status == true) {
                        window.location.href = "/Cart";
                    }
                }
            });
        });
    }
}
cart.init();
