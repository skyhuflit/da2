﻿$(function () {
    // Delete Modal ==
    $('#confirmDelete').on('show.bs.modal', function (e) {
        $dataName = $(e.relatedTarget).attr('data-name');
        $(this).find('.modal-body #dataName').text($dataName);

        // Pass form reference to modal for submission on yes/ok
        var form = $(e.relatedTarget).closest('form');
        $(this).find('.modal-footer #confirm').data('form', form);
    });

    // Form confirm (yes/ok) handler, submits form
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });

    $('#confirmBill').on('show.bs.modal', function (e) {
        $dataName = $(e.relatedTarget).attr('data-name');
        $(this).find('.modal-body #dataName').text($dataName);

        // Pass form reference to modal for submission on yes/ok
        var form = $(e.relatedTarget).closest('form');
        $(this).find('.modal-footer #confirm').data('form', form);
    });

    $('#confirmBill').find('.modal-footer #confirm').on('click', function () {
        $(this).data('form').submit();
    });

    // Datatables Config ==
    $('#table-data').DataTable({
        "order": [[0, "desc"]],
        "autoFill": true,
        "stateSave": true,
        "paging": true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ kết quả mỗi trang",
            "zeroRecords": "Không tìm thấy",
            "info": "Trang _PAGE_ / _PAGES_",
            "infoEmpty": "Không có kết quả",
            "infoFiltered": "(tìm kiếm trên _MAX_ kết quả)",
            "search": "Tìm kiếm"
        }, //language
    }); //datatable

    // Alert box ==
    $('.alert-position').removeClass('hide');
    $('.alert-position').delay(1500).slideUp(500);

});