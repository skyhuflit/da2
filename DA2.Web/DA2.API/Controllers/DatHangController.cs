﻿using DA2.API.Models;
using DA2.Data.Infrastructure;
using DA2.Data.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DA2.API.Controllers
{
    public class DatHangController : ApiController
    {
        private UnitOfWork _uow = new UnitOfWork();
        [HttpPost]
        [Route("api/DatHang/DatHangPost")]
        [ResponseType(typeof(string))]
        public string DatHangPost(string json)
        {
            //string example = "{\"customer_id\": 1,\"amount\": 100000,\"nguoi_nhan\": \"TVQ\",\"dia_chi\":\"HCM\",\"so_dt\": \"01222292364\",\"chitiethd\":[{\"product_id\": 10,\"quantity\": 3,\"price\": 500000}, 	{\"product_id\": 9,\"quantity\": 2,\"price\": 100000}]}";

            DatHangModel deserializeBill = JsonConvert.DeserializeObject<DatHangModel>(json);
            var bill = new BILL();
            bill.customer_id = deserializeBill.customer_id;
            bill.amount = deserializeBill.amount;
            bill.nguoi_nhan = deserializeBill.nguoi_nhan;
            bill.so_dt = deserializeBill.so_dt;
            bill.dia_chi = deserializeBill.dia_chi;
            bill.status = 0;
            bill.created = DateTime.Now;

            try
            {
                var newBillId = _uow.BillRepository.InsertBill(bill);
                foreach (var item in deserializeBill.chitiethd)
                {
                    var billitem = new BILLITEM();
                    billitem.bill_id = newBillId;
                    billitem.product_id = item.product_id;
                    billitem.price = item.price;
                    billitem.quantity = item.quantity;
                    _uow.BillItemRepository.Insert(billitem);
                }
                _uow.Save();
            }
            catch (Exception ex)
            {
                //throw (ex);
                return "Fail";
            }
            return "OK";
        }

        [HttpGet]
        public string DatHang()
        {
            string example = "{\"customer_id\": 1,\"amount\": 100000,\"nguoi_nhan\": \"TVQ\",\"dia_chi\":\"HCM\",\"so_dt\": \"01222292364\",\"chitiethd\":[{\"product_id\": 10,\"quantity\": 3,\"price\": 500000}, 	{\"product_id\": 9,\"quantity\": 2,\"price\": 100000}]}";

            DatHangModel deserializeBill = JsonConvert.DeserializeObject<DatHangModel>(example);
            var bill = new BILL();
            bill.customer_id = deserializeBill.customer_id;
            bill.amount = deserializeBill.amount;
            bill.nguoi_nhan = deserializeBill.nguoi_nhan;
            bill.so_dt = deserializeBill.so_dt;
            bill.dia_chi = deserializeBill.dia_chi;
            bill.status = 0;
            bill.created = DateTime.Now;

            try
            {
                var newBillId = _uow.BillRepository.InsertBill(bill);
                foreach (var item in deserializeBill.chitiethd)
                {
                    var billitem = new BILLITEM();
                    billitem.bill_id = newBillId;
                    billitem.product_id = item.product_id;
                    billitem.price = item.price;
                    billitem.quantity = item.quantity;
                    _uow.BillItemRepository.Insert(billitem);
                }
                _uow.Save();
            }
            catch (Exception ex)
            {
                //throw (ex);
                return "Fail";
            }
            return "OK";
        }
    }
}
