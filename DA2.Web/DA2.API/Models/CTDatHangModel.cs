﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DA2.API.Models
{
    public class CTDatHangModel
    {
        public int product_id { get; set; }
        public int quantity { get; set; }
        public float price { get; set; }
    }
}