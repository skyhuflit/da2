﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DA2.API.Models
{
    public class DatHangModel
    {
        public int customer_id { get; set; }
        public float amount { get; set; }
        public string nguoi_nhan { get; set; }
        public string dia_chi { get; set; }
        public string so_dt { get; set; }
        public List<CTDatHangModel> chitiethd { get; set; }
    }
}