namespace DA2.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRODUCT")]
    public partial class PRODUCT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT()
        {
            BILLITEMs = new HashSet<BILLITEM>();
            SHIPMENTs = new HashSet<SHIPMENT>();
        }

        public int id { get; set; }

        public int? brand_id { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [StringLength(100)]
        public string alias { get; set; }

        [StringLength(100)]
        public string image { get; set; }

        public double? price { get; set; }

        public int? stock { get; set; }

        public int? warranty { get; set; }

        [StringLength(200)]
        public string gift { get; set; }

        [Column(TypeName = "ntext")]
        public string specification { get; set; }

        [Column(TypeName = "ntext")]
        public string description { get; set; }

        public DateTime? created { get; set; }

        public bool? deleted { get; set; }

        [Column(TypeName = "date")]
        public DateTime? deleted_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BILLITEM> BILLITEMs { get; set; }

        public virtual BRAND BRAND { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SHIPMENT> SHIPMENTs { get; set; }
    }
}
