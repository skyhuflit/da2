namespace DA2.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SHIPMENT")]
    public partial class SHIPMENT
    {
        public int id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? date { get; set; }

        public int? product_id { get; set; }

        public int? quantity { get; set; }

        [StringLength(100)]
        public string supplier { get; set; }

        public virtual PRODUCT PRODUCT { get; set; }
    }
}
