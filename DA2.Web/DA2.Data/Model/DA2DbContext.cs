namespace DA2.Data.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DA2DbContext : DbContext
    {
        public DA2DbContext()
            : base("name=DA2DbContext")
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<BILL> BILLs { get; set; }
        public virtual DbSet<BILLITEM> BILLITEMs { get; set; }
        public virtual DbSet<BRAND> BRANDs { get; set; }
        public virtual DbSet<CATEGORY> CATEGORies { get; set; }
        public virtual DbSet<CUSTOMER> CUSTOMERs { get; set; }
        public virtual DbSet<PRODUCT> PRODUCTs { get; set; }
        public virtual DbSet<SHIPMENT> SHIPMENTs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<BILL>()
                .HasMany(e => e.BILLITEMs)
                .WithOptional(e => e.BILL)
                .HasForeignKey(e => e.bill_id);

            modelBuilder.Entity<BRAND>()
                .HasMany(e => e.PRODUCTs)
                .WithOptional(e => e.BRAND)
                .HasForeignKey(e => e.brand_id);

            modelBuilder.Entity<CATEGORY>()
                .HasMany(e => e.BRANDs)
                .WithOptional(e => e.CATEGORY)
                .HasForeignKey(e => e.category_id);

            modelBuilder.Entity<CUSTOMER>()
                .HasMany(e => e.BILLs)
                .WithOptional(e => e.CUSTOMER)
                .HasForeignKey(e => e.customer_id);

            modelBuilder.Entity<PRODUCT>()
                .HasMany(e => e.BILLITEMs)
                .WithOptional(e => e.PRODUCT)
                .HasForeignKey(e => e.product_id);

            modelBuilder.Entity<PRODUCT>()
                .HasMany(e => e.SHIPMENTs)
                .WithOptional(e => e.PRODUCT)
                .HasForeignKey(e => e.product_id);
        }
    }
}
