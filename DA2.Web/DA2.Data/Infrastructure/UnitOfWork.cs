﻿using DA2.Data.Model;
using DA2.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Infrastructure
{
    public class UnitOfWork : System.IDisposable
    {
        private DA2DbContext _dbContext = new DA2DbContext();
        private BillItemRepository _billItemRepository;
        public BillItemRepository BillItemRepository
        {
            get { return _billItemRepository ?? (_billItemRepository = new BillItemRepository(_dbContext)); }
        }

        private CategoryRepository _categoryRepository;
        
        public CategoryRepository CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new CategoryRepository(_dbContext)); }
        }


        private ShipmentRepository _shipmentRepository;
        public ShipmentRepository ShipmentRepository
        {
            get { return _shipmentRepository ?? (_shipmentRepository = new ShipmentRepository(_dbContext)); }
        }


        /* GenericRepository */
        //private GenericRepository<PRODUCT> _productRepository;
        //public GenericRepository<PRODUCT> ProductRepository
        //{
        //    get { return _productRepository ?? (_productRepository = new GenericRepository<PRODUCT>(_dbContext)); }
        //}

        /* CustomMoreRepository */

        private CustomerRepository _customerRepository;
        public CustomerRepository CustomerRepository
        {
            get
            {
                if (this._customerRepository == null)
                {
                    this._customerRepository = new CustomerRepository(_dbContext);
                }
                return _customerRepository;
            }
        }

        private BrandRepository _brandRepository;
        public BrandRepository BrandRepository
        {
            get
            {
                if (this._brandRepository == null)
                {
                    this._brandRepository = new BrandRepository(_dbContext);
                }
                return _brandRepository;
            }
        }

        private ProductRepository _productRepository;
        public ProductRepository ProductRepository
        {
            get
            {
                if (this._productRepository == null)
                {
                    this._productRepository = new ProductRepository(_dbContext);
                }
                return _productRepository;
            }
        }

        private BillRepository _billRepository;
        public BillRepository BillRepository
        {
            get
            {
                if (this._billRepository == null)
                {
                    this._billRepository = new BillRepository(_dbContext);
                }
                return _billRepository;
            }
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
