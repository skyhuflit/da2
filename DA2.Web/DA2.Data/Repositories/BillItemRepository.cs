﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class BillItemRepository : GenericRepository<BILLITEM>
    {
        public BillItemRepository(DA2DbContext context) : base(context)
        {
        }

        public List<BILLITEM> GetBillItemWithID(int? id)
        {
            return context.BILLITEMs.Where(m => m.bill_id == id).ToList();
        }

        public int? GetSoldItemAmount()
        {
            int? amount = 0;
            List<BILL> billList = context.BILLs.Where(m => m.status == 1).ToList();

            for (int i = 0; i < billList.Count; i++)
            {
                int id = billList[i].id;
                List<BILLITEM> itemList = context.BILLITEMs.Where(m => m.bill_id == id).ToList();
                for (int j = 0; j < itemList.Count; j++)
                {
                    amount += itemList[j].quantity;
                }
            }
            return amount;
        }
    }
}
