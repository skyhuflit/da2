﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class ShipmentRepository : GenericRepository<SHIPMENT>
    {
        public ShipmentRepository(DA2DbContext context) : base(context)
        {
        }

        public void Ship(SHIPMENT shipment)
        {
            Insert(shipment);

            PRODUCT p = context.PRODUCTs.Find(shipment.product_id);

            p.stock += shipment.quantity;

            context.PRODUCTs.Attach(p);
            context.Entry(p).State = EntityState.Modified;

            context.SaveChanges();

        }
    }
}
