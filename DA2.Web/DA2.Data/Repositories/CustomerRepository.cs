﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class CustomerRepository : GenericRepository<CUSTOMER>
    {
        public CustomerRepository(DA2DbContext context) : base(context)
        {
        }

        public List<CUSTOMER> GetAllActiveCustomer()
        {
            return context.CUSTOMERs.Where(m => m.deleted == false).ToList();
        }

        public void DeleteCustomer(CUSTOMER customer)
        {
            customer.deleted = true;
            customer.deleted_date = DateTime.Now.Date;

            Update(customer);
            context.SaveChanges();
        }
    }
}
