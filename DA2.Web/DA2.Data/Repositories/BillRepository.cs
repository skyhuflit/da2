﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class BillRepository : GenericRepository<BILL>
    {
        public BillRepository(DA2DbContext context) : base(context) { }
        public int InsertBill(BILL bill)
        {
            base.Insert(bill);
            return bill.id;
        }

        public void ConfirmBill(BILL bill, int status)
        {
            bill.status = status;
            Update(bill);
        }

        public List<BILL> GetConfirmedBill()
        {
            return context.BILLs.Where(m => m.status == 1).ToList();
        }
    }
}
