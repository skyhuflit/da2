﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class ProductRepository : GenericRepository<PRODUCT>
    {
        public ProductRepository(DA2DbContext context) : base(context) { }
        public PRODUCT NewestProduct()
        {
            return (from p in context.PRODUCTs
                    where p.deleted == false
                    orderby p.created
                    select p).Take(1).SingleOrDefault();
        }

        public List<PRODUCT> GetAllActiveProduct()
        {
            return context.PRODUCTs.Where(m => m.deleted == false).ToList();
        }

        public void DeleteProduct(int? id)
        {
            PRODUCT product = context.PRODUCTs.Find(id);
            product.deleted = true;
            product.deleted_date = DateTime.Now.Date;

            Update(product);
        }
    }
}
