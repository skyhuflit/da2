﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class CategoryRepository : GenericRepository<CATEGORY>
    {
        public CategoryRepository(DA2DbContext context) : base(context)
        {
        }

        public void DeleteCategory(CATEGORY category)
        {
            category.deleted = true;
            category.deleted_date = DateTime.Now.Date;

            Update(category);
            context.SaveChanges();
        }

        public List<CATEGORY> GetAllActiveCategory()
        {
            return context.CATEGORies.Where(m => m.deleted == false).ToList();
        }
    }
}
