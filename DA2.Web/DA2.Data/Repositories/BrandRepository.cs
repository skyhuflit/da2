﻿using DA2.Data.Infrastructure;
using DA2.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA2.Data.Repositories
{
    public class BrandRepository : GenericRepository<BRAND>
    {
        public BrandRepository(DA2DbContext context) : base(context) { }

        public void DeleteBrand(BRAND brand)
        {
            brand.deleted = true;
            brand.deleted_date = DateTime.Now.Date;

            Update(brand);
        }

        public List<BRAND> GetAllActiveBrand()
        {
            return context.BRANDs.Where(m => m.deleted == false).ToList();
        }
    }
}
