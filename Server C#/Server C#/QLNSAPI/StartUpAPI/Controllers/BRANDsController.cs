﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using StartUpAPI.Models;

namespace StartUpAPI.Controllers
{
    public class BRANDsController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/BRANDs
        public IQueryable<BRAND> GetBRANDs()
        {
            return db.BRANDs.Where(s=>s.deleted == false);
        }

        // GET: api/BRANDs/5
        [ResponseType(typeof(BRAND))]
        public IHttpActionResult GetBRAND(int id)
        {
            BRAND bRAND = db.BRANDs.Find(id);
            if (bRAND == null)
            {
                return NotFound();
            }

            return Ok(bRAND);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BRANDExists(int id)
        {
            return db.BRANDs.Count(e => e.id == id) > 0;
        }
    }
}