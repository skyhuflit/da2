﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using StartUpAPI.Models;

namespace StartUpAPI.Controllers
{
    public class BILLsController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/BILLs
        public IQueryable<BILL> GetBILLs()
        {
            return db.BILLs;
        }

        //get history
        [ResponseType(typeof(BILL))]
        [Route("api/BILLs/GetHistoryOrder/{id}")]
        public IQueryable<BILL> GetHistoryOrder(String id)
        {
            return db.BILLs.Where(s => s.customer_id.ToString() == id);
        }


        // GET: api/BILLs/5
        [ResponseType(typeof(BILL))]
        public IHttpActionResult GetBILL(int id)
        {
            BILL bILL = db.BILLs.Find(id);
            if (bILL == null)
            {
                return NotFound();
            }

            return Ok(bILL);
        }

        // POST: api/BILLs
        [ResponseType(typeof(BILL))]
        public IHttpActionResult PostBILL(BILL bILL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bILL.created = DateTime.Now;
            db.BILLs.Add(bILL);

            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = bILL.id }, bILL);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BILLExists(int id)
        {
            return db.BILLs.Count(e => e.id == id) > 0;
        }
    }
}