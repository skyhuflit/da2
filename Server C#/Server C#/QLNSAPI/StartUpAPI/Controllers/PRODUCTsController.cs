﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using StartUpAPI.Models;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Net.Http.Headers;

namespace StartUpAPI.Controllers
{
    public class PRODUCTsController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/PRODUCTs
        public IQueryable<PRODUCT> GetPRODUCTs()
        {
           
            return db.PRODUCTs.Where(s=>s.deleted==false);
        }

        public IQueryable<PRODUCT> GetTOPPRODUCTs()
        {
            return db.PRODUCTs.Where(s => s.deleted == false).Take(6);
        }
        //get product new
        public IQueryable<PRODUCT> GetProductNew()
        {
            return db.PRODUCTs.OrderByDescending(s => s.created).Where(s => s.deleted == false);
        }


        // GET: api/PRODUCTs/5
        [ResponseType(typeof(PRODUCT))]
        public IHttpActionResult GetPRODUCT(int id)
        {
            PRODUCT pRODUCT = db.PRODUCTs.Find(id);
            if (pRODUCT == null)
            {
                return NotFound();
            }

            return Ok(pRODUCT);
        }

        [ResponseType(typeof(PRODUCT))]
        [Route("api/PRODUCTs/SearchProduct/{name}")]
        public IQueryable<PRODUCT> GetSearchProduct(string name)
        {
            return db.PRODUCTs.Where(s => (s.name.Equals(name.ToString()) || (s.name.Contains(name.ToString()))));
        }


        //get product theo the loai
        [ResponseType(typeof(PRODUCT))]
        [Route("api/PRODUCTs/GetProductOfType/{id}")]
        public IQueryable<PRODUCT> GetProductOfType(string id)
        {
            return db.PRODUCTs.Where(s => (s.brand_id.ToString() == id) && (s.deleted == false));
        }


        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCTExists(int id)
        {
            return db.PRODUCTs.Count(e => e.id == id) > 0;
        }

        [HttpGet]
        public HttpResponseMessage GetProductImage(string id)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();

            // Photo.Resize is a static method to resize the image
            PRODUCT ev = db.PRODUCTs.Find(id);
            string root = ev.image;
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            FileStream fileStream = new FileStream(root, FileMode.Open);
            Image image = Image.FromStream(fileStream);
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Jpeg);
            var byteArrayContent = new ByteArrayContent(memoryStream.ToArray());
            byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            result.Content = byteArrayContent;
            memoryStream.Close();
            fileStream.Close();
            return result;
        }

        public void Log(string logMessage, TextWriter w)
        {
            w.Write("Log PostImage : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("Error             : {0}", logMessage);
            w.WriteLine("-------------------------------");
        }

    }
}