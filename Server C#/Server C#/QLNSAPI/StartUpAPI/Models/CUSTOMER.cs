namespace StartUpAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CUSTOMER")]
    public partial class CUSTOMER
    {
        

        public int id { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(100)]
        public string password { get; set; }

        [StringLength(100)]
        public string tel { get; set; }

        [StringLength(100)]
        public string address { get; set; }

        [Column(TypeName = "date")]
        public DateTime? created { get; set; }

        public bool? deleted { get; set; }

        [Column(TypeName = "date")]
        public DateTime? deleted_date { get; set; }

    }
}
