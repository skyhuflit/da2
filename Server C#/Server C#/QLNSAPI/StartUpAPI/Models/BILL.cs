namespace StartUpAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BILL")]
    public partial class BILL
    {
        public int id { get; set; }

        public int? status { get; set; }

        public DateTime? created { get; set; }

        public int? customer_id { get; set; }

        public double? amount { get; set; }

        [StringLength(50)]
        public string nguoi_nhan { get; set; }

        [StringLength(200)]
        public string dia_chi { get; set; }

        [StringLength(20)]
        public string so_dt { get; set; }

     
    }
}
