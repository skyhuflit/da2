namespace StartUpAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BILLITEM")]
    public partial class BILLITEM
    {
        public int id { get; set; }

        public int? bill_id { get; set; }

        public int? product_id { get; set; }

        public int? quantity { get; set; }

        public double? price { get; set; }

    }
}
